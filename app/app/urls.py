import debug_toolbar
from django.contrib import admin
from django.urls import path, include
from shortener.views import short_url_redirect

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/user/', include('user.urls')),
    path('api/v1/', include('shortener.urls')),
    path('r/<slug:short_url>', short_url_redirect),
    path('__debug__/', include(debug_toolbar.urls)),

]
