from django.contrib import admin

from shortener.models import Url, Log

admin.site.register(Url)
admin.site.register(Log)
# Register your models here.
