from datetime import datetime, timedelta

from django.conf import settings
from django.db import models


def auto_expiration_date():
    return datetime.now() + timedelta(**settings.DEFAULT_URL_EXPIRATION)


class Url(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    origin_url = models.CharField(max_length=512)
    short_url = models.CharField(max_length=16, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    expiration_date = models.DateTimeField(default=auto_expiration_date)

    @property
    def is_expired(self):
        if self.expiration_date < datetime.now():
            return True
        return False

    def __str__(self):
        return self.short_url


class Log(models.Model):
    url = models.ForeignKey(Url, on_delete=models.CASCADE)
    visited_at = models.DateTimeField(auto_now_add=True)
    is_pc = models.BooleanField(default=True)
    browser = models.CharField(max_length=255)

    def __str__(self):
        return str(self.url.short_url) + "@" + str(self.visited_at)
