from django.core.validators import URLValidator
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from shortener.models import Url


class CreateURLSerializer(serializers.Serializer):
    origin_url = serializers.CharField(max_length=512)
    expiration_date = serializers.DateTimeField(required=False, default=None)
    alias_name = serializers.CharField(max_length=16, required=False, default=None)
    short_url = serializers.CharField(max_length=16, read_only=True)

    def validate(self, attrs):
        url_validator = URLValidator()
        url_validator(attrs['origin_url'])
        if attrs["alias_name"]:
            if Url.objects.filter(short_url=attrs['alias_name']).exists():
                raise ValidationError("alias name is already used")
        return attrs



