import hashlib
import base64
import re
import uuid


def create_short_url():
    uuid_ = uuid.uuid4()
    hash = hashlib.sha256(str(uuid_).encode())
    hash_base64 = base64.b64encode(hash.digest())
    return re.sub('[^A-Za-z0-9]+', '', hash_base64.decode())[:8]
