from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from shortener.models import Url

CREATE_URL = reverse("shortener:url-list")


class UrlTests(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(email="test@jl.com", password='testpass')
        self.client.force_authenticate(user=self.user)

    def test_create_short_url_api(self):
        payload = {"origin_url": "http://test.com"}
        res = self.client.post(CREATE_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(Url.objects.all()), 1)
        self.assertIs(Url.objects.filter(origin_url=payload["origin_url"]).exists(), True)

