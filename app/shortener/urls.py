from django.urls import include, path
from rest_framework import routers

from shortener import views

app_name = "shortener"

router = routers.DefaultRouter()
router.register("create-url", views.CreateURLViewSet, 'url')
router.register("analytics", views.AnalyticsViewSet, 'analytic')

urlpatterns = [
    path("", include(router.urls)),
]
