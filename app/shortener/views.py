from datetime import datetime, timedelta

from django.db.models import Count, Q, Sum
from django.shortcuts import get_object_or_404, redirect
from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.utils import json

from shortener import serializers
from .models import Url, Log
from .services import create_short_url


class CreateURLViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    serializer_class = serializers.CreateURLSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        data = serializer.validated_data
        if data['alias_name']:
            short_url = data['alias_name']
        else:
            short_url = create_short_url()
        if data['expiration_date']:
            created_url = Url.objects.create(user=self.request.user, short_url=short_url, origin_url=data['origin_url'],
                                             expiration_date=data['expiration_date'])
        else:
            created_url = Url.objects.create(user=self.request.user, short_url=short_url, origin_url=data['origin_url'])

        serializer.validated_data['short_url'] = short_url
        serializer.validated_data['expiration_date'] = created_url.expiration_date


def short_url_redirect(request, short_url):
    queryset = Url.objects.filter(short_url=short_url)
    url = get_object_or_404(queryset)
    is_pc = request.user_agent.is_pc
    browser = request.user_agent.browser.family
    Log.objects.create(url=url, is_pc=is_pc, browser=browser)
    return redirect(url.origin_url)


class AnalyticsViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Log.objects.all()

    def get_queryset(self):
        queryset = self.queryset.filter(url__user=self.request.user)
        print("****")
        print(queryset)
        print("****")
        time = self.request.query_params.get('time', None)
        start_of_today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        if time == "last-day":
            time_delta = start_of_today - timedelta(days=1)
        elif time == "last-week":
            time_delta = start_of_today - timedelta(days=7)
        elif time == "last-month":
            time_delta = start_of_today - timedelta(days=30)
        else:
            time_delta = datetime.now()

        return queryset.filter(visited_at__range=(min(start_of_today, time_delta), max(start_of_today, time_delta)))

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        print(queryset)
        total_counts = queryset.values("url__short_url").annotate(total_count=Count("url"))
        pc_counts = queryset.values("url__short_url", "is_pc").annotate(pc_count=Count("url"))
        browser_counts = queryset.values("url__short_url", "browser").annotate(browser_count=Count("url"))
        urls_list = []
        for url in total_counts:
            url_item = dict()
            short_url = url["url__short_url"]
            url_item["short_url"] = short_url
            # url_item["long_url"] = url.url.origin_url
            url_item["total_visits"] = url["total_count"]
            for i in pc_counts:
                if i["url__short_url"] == short_url and i["is_pc"]:
                    url_item["pc"] = i["pc_count"]
                    url_item["mobile"] = url_item["total_visits"] - url_item["pc"]
                    break
            for i in browser_counts:
                if i["url__short_url"] == short_url:
                    url_item[i["browser"]] = i["browser_count"]

            urls_list.append(url_item)

        return Response(urls_list)
